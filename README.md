

# Game Of Reading And Discussing
 
This game is used in the Serious Games HiG course.



# Rules of the game

* each week students read articles provided for a given week
* each student can post one question related to each of the articles
* each student then votes on the questions posted for a given article
* top 5 questions are selected and authors of these questions receive the points allocated to those questions
* each student then posts answers to one question per article (pick 1 question out of 5 the best, see point above)
* each student then votes on the answers; authors of the highly ranked answers receive the points allocated to them




# Implementation of the system

## Getting started

* Install nodejs on your system: [NodeJS homepage](http://nodejs.org/)
* inside the gorad/client run the following

    * npm install
    * bower update
    * grunt
    * grunt jade

* Now, all your environments and dependencies should be sorted out, and you should have the running app in your browser after running:

    * grunt serve
 

## Software stack

    * Programming language: CoffeeScript 
    * Boiler plate code generator/workflow: yeoman - http://yeoman.io/
    * Package manager(s): bower - http://bower.io/  AND npm - http://nodejs.org
    * Module system: RequireJS - http://requirejs.com
    * Build system: Grunt - http://gruntjs.com/
    * Templates: Jade - http://jade-lang.com/
    * Data binding: AngularJS - http://angularjs.org/
    * Sync: AngularJS
    * Widgets: Bootstrap3 - http://getbootstrap.com/
    * Test framework: Karma + Jasmin
   

## Details

The system is currently being implemented in CoffeeScript and AngularJs. The main goal is to 
highlight some of the feature of modern software stack:
    * coffeescript and JSON as the main implementation language and data passing mechanism
    * AngularJS for front-end web application development
    * Firebase for application hosting and data storage
    * grunt, bower and yeoman for automation, dependency tracking and workflow


## System design

 
 * Users
    * Name, credentials
    * Role: (possible: admin, teacher, student)
 * Article
 	* List of tags (textual tagging)
    * List of Questions
       * Answers

## Tags

Tags should be globally unique. Prefix your tags with a reverse domain name prefix, eg. 
no.hig.imt4102.week3

Each article is publically visible, and can be tagged for different courses. 



# Authors

Game Design:

    Simon, Rune, Mariusz
	
Software Implementation:

    Mariusz <mariusz.nowostawski@hig.no>



# License

Copyright 2013-2014 University College Gjovik, HiG

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
