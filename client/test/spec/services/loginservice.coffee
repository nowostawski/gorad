'use strict'

describe 'Service: loginService', () ->

  # load the service's module
  beforeEach module 'goradApp'

  # instantiate service
  loginService = {}
  beforeEach inject (_loginService_) ->
    loginService = _loginService_

  describe 'loginService', () ->
    beforeEach module ($provide) ->
      $provide.value 'Firebase', firebaseStub()
      $provide.value '$location', stub('path')
      $provide.value '$firebaseSimpleLogin', angularAuthStub()
      $provide.value 'firebaseRef', firebaseStub()

    describe '#login', () ->
      it 'should return error if $firebaseSimpleLogin.$login fails',
      inject ($q, $timeout, loginService, $firebaseSimpleLogin) ->
        cb = jasmine.createSpy()
        loginService.init('/login')
        $firebaseSimpleLogin.fns.$login.andReturn(reject($q, 'test_error'))
        loginService.login('test@test.com', '123', cb)
        flush($timeout)
        expect(cb).toHaveBeenCalledWith 'test_error'

