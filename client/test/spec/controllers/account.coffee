'use strict'

describe 'Controller: AccountCtrl', () ->

  # load the controller's module
  beforeEach module 'goradApp'

  AccountCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope, _syncData_) ->
    scope = $rootScope.$new()
    AccountCtrl = $controller 'AccountCtrl', {
      $scope: scope
    }

  it 'should be active for menu: account', () ->
    expect(scope.isActive('account')).toBeTruthy

  it 'should not be active for menu: home', () ->
    expect(scope.isActive('home')).toBeFalsy
