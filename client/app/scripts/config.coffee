'use strict'

angular.module('goradApp.config', [])
  .constant('version', '0.2')
  .constant('loginRedirectPath', '/login')
  .constant('FBURL', 'https://glowing-fire-1222.firebaseio.com')
