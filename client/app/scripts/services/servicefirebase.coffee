'use strict'

pathRef = (args) ->
  for i,v in args
    if typeof(v) is 'object'
      args[i] = pathRef v
  return args.join '/'


angular.module('goradApp.service.firebase', ['firebase'])

  # a simple utility to create references to Firebase paths
  .factory('firebaseRef', ['Firebase', 'FBURL', (Firebase, FBURL) ->
    #
    #   * @param {String|Array...} path
    #   * @return a Firebase instance
    #
    return (path) ->
      ref = new Firebase FBURL
      args = Array.prototype.slice.call arguments
      if args.length
        ref = ref.child pathRef(args)

      return ref
  ])

  # a simple utility to create $firebase objects from angularFire
  .service('syncData', ['$firebase', 'firebaseRef', ($firebase, firebaseRef) ->
    return (path, props) ->
      ref = firebaseRef path
      props = angular.extend {}, props
      angular.forEach ['limit', 'startAt', 'endAt'], (k) ->
        if props.hasOwnProperty k
          v = props[k]
          ref = ref[k].apply(ref, angular.isArray(v)? v : [v])
          delete props[k]

      return $firebase(ref, props)
  ])

