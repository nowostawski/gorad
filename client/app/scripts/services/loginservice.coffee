'use strict'

angular.module('goradApp.service.login',
  ['goradApp.service.firebase'])
  .factory('loginService', [
    '$rootScope', '$firebaseAuth', 'firebaseRef', 'profileCreator', '$timeout', '$location',
    ($rootScope, $firebaseAuth, firebaseRef, profileCreator, $timeout, $location) ->


      return {
        init: () ->
          ref = new Firebase "https://glowing-fire-1222.firebaseio.com"
          $rootScope.auth = $firebaseAuth ref
          this

        login: (email, pass, callback) ->
          $rootScope.auth.$authWithPassword
            email: email,
            password: pass,
          .then (authData) ->
            $location.path '/home'
            callback null, authData
          .catch (error) ->
            callback error



        logout: () ->
          $rootScope.auth.$unauth()


        changePassword: (opts) ->
          # assertAuth()
          cb = opts.callback || (err) ->
              console.log err

          if not opts.oldpass || not opts.newpass
            cb 'Error: Please enter a password.'
          else if opts.newpass != opts.newpass_confirm
            cb 'Error: Passwords do not match.'
          else
            $rootScope.auth.$changePassword(
              email: opts.email
              oldPassword: opts.oldpass
              newPassword: opts.newpass
            ).then(() -> cb 'Password changed!'
            ).catch (err ) -> cb err


        createAccount: (email, pass, callback) ->
          $rootScope.auth.$createUser(
            email: email
            password: pass
          ).then( (userdata) ->
            return $rootScope.auth.$authWithPassword
              email: email
              password: pass
          ).then( (authData) ->
              profileCreator authData.uid, email, () ->
                callback null, authData
          ).catch (err) ->
            callback err, null

        createProfile: profileCreator
      }

    ])

  .factory('profileCreator', [
    'firebaseRef', '$timeout', (firebaseRef, $timeout) ->

      ucfirst = (str) ->
        # credits: http://kevin.vanzonneveld.net
        str += ''
        f = str.charAt(0).toUpperCase()
        return f + str.substr(1)

      firstPartOfEmail = (email) ->
        return ucfirst(email.substr(0, email.indexOf('@')) || '')

      (id, email, callback) ->
        firebaseRef('users/'+id).set({
          email: email,
          name: firstPartOfEmail(email)
        }, (err) ->
            #err && console.error(err);
            if callback
              $timeout () ->
                callback err
        )
  ])

