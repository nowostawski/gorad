'use strict'

angular.module('goradApp', [
  'firebase'
  'goradApp.config'
  'goradApp.service.firebase'
  'goradApp.service.login'
  'ngCookies'
  'ngResource'
  'ngSanitize'
  'ngRoute'

]).factory("Auth", ["$firebaseAuth", "FBURL", ($firebaseAuth, FBURL) ->
  ref = new Firebase FBURL
  return $firebaseAuth ref

]).config(($routeProvider, $locationProvider) ->

  $routeProvider

    .when '/',
      templateUrl: 'views/main.html'
      controller: 'MainCtrl'

    .when '/login',
      templateUrl: 'views/login.html'
      controller: 'MainCtrl'

    .when '/account',
      templateUrl: 'views/account.html'
      controller: 'AccountCtrl'
      resolve:
        "currentAuth": ["Auth", (Auth) ->
          return Auth.$waitForAuth()
        ]

    .when '/home',
      templateUrl: 'views/home.html'
      controller: 'HomeCtrl'
      resolve:
        "currentAuth": ["Auth", (Auth) ->
          return Auth.$waitForAuth()
        ]

    .when '/articles',
      templateUrl: 'views/list_articles.html'
      controller: 'ListArticlesCtrl'
      resolve:
        "currentAuth": ["Auth", (Auth) ->
          return Auth.$waitForAuth()
        ]

    .when '/vote',
      templateUrl: 'views/vote.html'
      controller: 'VoteCtrl'
      resolve:
        "currentAuth": ["Auth", (Auth) ->
          return Auth.$waitForAuth()
        ]

  .otherwise redirectTo: '/'

  $locationProvider
    .html5Mode(true)
    .hashPrefix('!')

).run([
  'loginService', '$rootScope', 'FBURL', (loginService, $rootScope, FBURL) ->
    loginService.init '/login'
    $rootScope.FBURL = FBURL
])

