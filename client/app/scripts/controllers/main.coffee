'use strict'

angular.module('goradApp')
  .controller 'MainCtrl', ($rootScope, $scope, $location, loginService) ->

    assertValidLoginAttempt = () ->
      if not $scope.email?
        $scope.err = 'Please enter an email address'
      else if not $scope.pass?
        $scope.err = 'Please enter a password'
      else if $scope.pass isnt $scope.pass_confirm
        $scope.err = 'Passwords do not match'

      return not $scope.err?

    $scope.cancelButton = () ->
      $scope.createMode = false
      $scope.pass = ''
      $scope.pass_confirm = ''
      $scope.err = null

    $scope.login = (cb) ->
      $scope.err = null
      if !$scope.email
        $scope.err = 'Please enter an email address'
      else if !$scope.pass
        $scope.err = 'Please enter a password'
      else
        loginService.login $scope.email, $scope.pass, (err, user) ->
          if err
            $scope.err = err + ''
          else
            $scope.err = null
            if cb?
              cb(user)
            else
              $rootScope.$emit "gorad:login"
              $location.path '/home'

    $scope.createAccount = () ->
      $scope.err = null
      if assertValidLoginAttempt()
        loginService.createAccount $scope.email, $scope.pass, (err, userData) ->
          if err
            $scope.err = err
          else
            $location.path '/account'


angular.module('goradApp')
  .service 'sharedProperties', () ->
    properties = {}

    {
      getProperty: (name) ->
        return properties[name]

      setProperty: (name, value) ->
        properties[name] = value
    }
