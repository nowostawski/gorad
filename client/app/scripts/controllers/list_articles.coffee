'use strict'

angular.module('goradApp')
.controller 'ListArticlesCtrl', [
    '$scope', '$location', '$firebase', 'firebaseRef', 'currentAuth', 'sharedProperties',
    ($scope, $location, $firebase, firebaseRef, currentAuth, sharedProperties) ->

      $scope.auth = {}
      $scope.user = $firebase(firebaseRef('users', currentAuth.uid)).$asObject()
      $scope.auth.user = $scope.user

      $scope.article = {} # article placeholder, for adding new articles.

      $scope.question = {}
      $scope.weekEnabled = {}

      $scope.articles = $firebase(firebaseRef 'articles').$asObject()

      $scope.isActive = (path) ->
        path is 'article'

      $scope.addArticle = () ->
        $scope.articles.$add
          title: $scope.article.title
          author: $scope.article.author
          publishedin: $scope.article.publishedin
          year: $scope.article.year
          url: $scope.article.url
          week: $scope.article.week
          tags: $scope.article.tags
        $scope.article = {}

  ]
