'use strict'

angular.module('goradApp')
.controller 'VoteCtrl', [
    '$scope', '$location', '$firebase', 'syncData', 'sharedProperties',
    ($scope, $location, $firebase, syncData, sharedProperties) ->


      syncData(['users', $scope.auth.user.uid]).$bind($scope, 'user')


      id = 'https://glowing-fire-1222.firebaseio.com/articles/' +
        sharedProperties.getProperty 'article_id'


      $firebase(new Firebase(id)).$bind($scope, 'article').then () ->
        questions = $scope.article['questions']
        for key of questions
          q = questions[key]
          if not q[$scope.auth.user.uid]
            $scope.article.$child('questions').$child(key)
              .$child($scope.auth.user.uid).$set {score: 0}

      $scope.isActive = (path) ->
        path is 'article'


      $scope.gotoArticles = () ->
        $location.path '/article'



  ]