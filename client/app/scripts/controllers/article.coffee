'use strict'

angular.module('goradApp')
.controller 'ArticleCtrl', [
    '$scope', '$location', '$firebase', 'firebaseRef', 'currentAuth', 'sharedProperties',
    ($scope, $location, $firebase, firebaseRef, currentAuth, sharedProperties) ->

      $scope.auth = {}
      $scope.user = $firebase(firebaseRef('users', currentAuth.uid)).$asObject()
      $scope.auth.user = $scope.user

      $scope.article = { tags: 'IMT4102, serious games'}
      $scope.question = {}
      $scope.weekEnabled = {}



      $scope.articles = $firebase(firebaseRef 'articles').$asObject()

#      for key in Object.keys($scope.articles)
#        # Initialize placeholders for questions
#        a = $scope.articles[key]
#        continue if not a? or not a.$id?
#        $scope.question[a.$id] = {}
#        if a['questions']?
#          q = a['questions']
#          for id of q
#            if q[id].userid is $scope.auth.user.uid
#              # locking the placeholder so that
#              # there is only 1 question per user.uid
#              $scope.question[a.$id] = null
#
#
#      $scope.getWeeks = () ->
#        if not $scope.articles? then return []
#        weeks = []
#        for key in Object$scope.articles.$getIndex()
#          a = $scope.articles[key]
#          if not a? then return []
#          a.$id = key
#          if a?.week? and not (a.week in weeks)
#            weeks.push a.week
#            if $scope.weekEnabled[a.week] is null or $scope.weekEnabled[a.week] is undefined
#              $scope.weekEnabled[a.week] = true
#        return weeks
#
#      $scope.getArticlesForWeek = (week) ->
#        if not $scope.articles? then return []
#        articles = []
#        for key in $scope.articles.$getIndex()
#          a = $scope.articles[key]
#          if a?.week? and a.week is week
#            articles.push a
#        return articles


      $scope.isActive = (path) ->
        path is 'article'


      $scope.addArticle = () ->
        $scope.articles.$add
          title: $scope.article.title
          author: $scope.article.author
          publishedin: $scope.article.publishedin
          year: $scope.article.year
          url: $scope.article.url
          week: $scope.article.week
          tags: $scope.article.tags
        $scope.article = {}


      $scope.addQuestion = (article_id) ->
        question = $scope.articles.$child(article_id).$child('questions')
        question.$add
          text: $scope.question[article_id].text
          score: 0
          userid: $scope.auth.user.uid
        $scope.question[article_id] = null


      $scope.scoreQuestions = (article_id) ->
        sharedProperties.setProperty 'article_id', article_id
        $location.path '/vote'
  ]
