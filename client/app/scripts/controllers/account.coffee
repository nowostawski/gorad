'use strict'

angular.module('goradApp')
  .controller 'AccountCtrl', [
    '$scope', '$firebase', 'loginService', 'firebaseRef', 'currentAuth', '$location',
    ($scope, $firebase, loginService, firebaseRef, currentAuth, $location) ->

      $firebase(firebaseRef('users', currentAuth.uid)).$asObject().$bindTo($scope, 'user')

      $scope.logout = () ->
        loginService.logout()
        $location.path '/'

      $scope.oldpass = null
      $scope.newpass = null
      $scope.newpass_confirm = null

      $scope.reset = () ->
        console.log "scope reset in AccountCtrl"
        $scope.err = null
        $scope.msg = null

      buildPwdParms = () ->
        return {
          email: $scope.user.email
          oldpass: $scope.oldpass
          newpass: $scope.newpass
          newpass_confirm: $scope.newpass_confirm
          callback: (err) ->
            if err
              $scope.msg = err
            else
              $scope.oldpass = null
              $scope.newpass = null
              $scope.newpass_confirm = null
              $scope.msg = 'Password updated!'
        }

      $scope.updatePassword = () ->
        $scope.reset()
        loginService.changePassword(buildPwdParms())

      $scope.isActive = (path) ->
        path is 'account'

    ]
