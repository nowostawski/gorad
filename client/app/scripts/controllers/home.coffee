'use strict'

angular.module('goradApp')
  .controller 'HomeCtrl', [
    '$scope', '$firebase', 'firebaseRef', 'currentAuth',
    ($scope, $firebase, firebaseRef, currentAuth) ->

      $scope.auth = {}
      $scope.user = $firebase(firebaseRef('users', currentAuth.uid)).$asObject()
      $scope.auth.user = $scope.user

      $scope.isActive = (path) ->
        path is 'home'

    ]
