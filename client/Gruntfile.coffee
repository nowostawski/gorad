'use strict'

module.exports = (grunt) ->

  require('load-grunt-tasks')(grunt)
  require('time-grunt')(grunt)

  # Connect middleware for rewriting the AngularJS routes
  # to point back to the index.html page
  fs = require 'fs'
  url = require 'url'

  urlRewrite = (connect, options, rewriteFiles) ->
    indexFile = 'app/index.html'
    rerouting = (req, res, next) ->
      path = url.parse(req.url).pathname
      if path in rewriteFiles
        fs.readFile indexFile, (error, buffer) ->
          return next(error) if error
          resp = {
            headers:
              'Content-Type': 'text/html'
              'Content-Length': buffer.length
            body: buffer
          }
          res.writeHead 200, resp.headers
          res.end resp.body
      else
        return next()
    staticRoutes = []
    for route in options.base
      staticRoutes.push connect.static(route)

    return [ rerouting ].concat staticRoutes




  grunt.initConfig

    yeoman:
      app: require('./bower.json').appPath || 'app'
      dist: 'dist'

    watch:
      coffee:
        files: ['<%= yeoman.app %>/scripts/{,*/}*.{coffee,litcoffee,coffee.md}']
        tasks: ['newer:coffee:dist']

      coffeeTest:
        files: ['test/spec/{,*/}*.{coffee,litcoffee,coffee.md}']
        tasks: ['newer:coffee:test', 'karma']

      styles:
        files: ['<%= yeoman.app %>/styles/{,*/}*.css']
        tasks: ['newer:copy:styles', 'autoprefixer']

      gruntfile:
        files: ['Gruntfile.js']

      jade:
        files:  [
          '<%= yeoman.app %>/*.jade'
          '<%= yeoman.app %>/views/**/*.jade'
        ]
        tasks: 'jade'

      livereload:
        options:
          livereload: '<%= connect.options.livereload %>'

        files: [
          '<%= yeoman.app %>/{,*/}*.html'
          '.tmp/styles/{,*/}*.css'
          '.tmp/scripts/{,*/}*.js'
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]

    connect:
      options:
        port: 9000
        hostname: 'localhost'
        livereload: 35729
        middleware: (connect, options) ->
          return urlRewrite connect, options, [
            '/home'
            '/login'
            '/account'
            '/article'
          ]

      livereload:
        options:
          open: true,
          base: [
            '.tmp'
            '<%= yeoman.app %>'
          ]

      test:
        options:
          port: 9001
          base: [
            '.tmp'
            'test'
            '<%= yeoman.app %>'
          ]

    configureRewriteRules:
      options:
        rulesProvider: 'connect.rules'

    dist:
      options:
        base: '<%= yeoman.dist %>'

    coffeelint:
      app: ['<%= yeoman.app %>/**/*.coffee', 'test/**/*.coffee']


    jshint:
      options:
        jshintrc: '.jshintrc'
        reporter: require('jshint-stylish')
      all: [
        'Gruntfile.js'
      ]


    clean:
      dist:
        files: [
          dot: true
          src: [
            '.tmp'
            '<%= yeoman.dist %>/*'
            '!<%= yeoman.dist %>/.git*'
            '<%= yeoman.app %>/views/*.html'
          ]
        ]

      server: '.tmp'


    autoprefixer:
      options:
        browsers: ['last 1 version']
      dist:
        files: [
          expand: true,
          cwd: '.tmp/styles/'
          src: '{,*/}*.css'
          dest: '.tmp/styles/'
        ]


    'bower-install':
      app:
        html: '<%= yeoman.app %>/index.html'
        ignorePath: '<%= yeoman.app %>/'
        src: ['<%= yeoman.app %>/index.html']
        cwd: ''


    coffee:
      options:
        sourceMap: true
        sourceRoot: ''
      dist:
        files: [
          expand: true
          cwd: '<%= yeoman.app %>/scripts'
          src: '{,*/}*.coffee'
          dest: '.tmp/scripts'
          ext: '.js'
        ]
      test:
        files: [
          expand: true
          cwd: 'test/spec'
          src: '{,*/}*.coffee'
          dest: '.tmp/spec'
          ext: '.js'
        ]

    jade:
      compile:
        options:
          data: { debug: false }
          pretty: true
        files: [
          expand: true
          cwd: '<%= yeoman.app %>/views'
          src: ['**/*.jade']
          dest: '<%= yeoman.app %>/views'
          ext: '.html'
        ]
      dist:
        options:
          data: { debug: false }
          pretty: false
        files: [
          expand: true
          cwd: '<%= yeoman.app %>/views'
          src: ['**/*.jade']
          dest: '<%= yeoman.dist %>/views'
          ext: '.html'
        ]

    rev:
      dist:
        files:
          src: [
            '<%= yeoman.dist %>/scripts/{,*/}*.js'
            '<%= yeoman.dist %>/styles/{,*/}*.css'
            '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
            '<%= yeoman.dist %>/styles/fonts/*'
          ]

    useminPrepare:
      html: '<%= yeoman.app %>/index.html'
      options:
        dest: '<%= yeoman.dist %>'


    usemin:
      html: ['<%= yeoman.dist %>/{,*/}*.html']
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css']
      options:
        assetsDirs: ['<%= yeoman.dist %>']


    imagemin:
      dist:
        files: [
          expand: true,
          cwd: '<%= yeoman.app %>/images'
          src: '{,*/}*.{png,jpg,jpeg,gif}'
          dest: '<%= yeoman.dist %>/images'
        ]

    svgmin:
      dist:
        files: [
          expand: true
          cwd: '<%= yeoman.app %>/images'
          src: '{,*/}*.svg'
          dest: '<%= yeoman.dist %>/images'
        ]

    htmlmin:
      dist:
        options:
          collapseWhitespace: true
          collapseBooleanAttributes: true
          removeCommentsFromCDATA: true
          removeOptionalTags: true
        files: [
          expand: true,
          cwd: '<%= yeoman.dist %>'
          src: ['*.html', 'views/{,*/}*.html']
          dest: '<%= yeoman.dist %>'
        ]


    ngmin:
      dist:
        files: [
          expand: true,
          cwd: '.tmp/concat/scripts'
          src: '*.js'
          dest: '.tmp/concat/scripts'
        ]


    cdnify:
      dist:
        html: ['<%= yeoman.dist %>/*.html']

    copy:
      dist:
        files: [{
          expand: true
          dot: true
          cwd: '<%= yeoman.app %>'
          dest: '<%= yeoman.dist %>'
          src: [
            '*.{ico,png,txt}'
            '.htaccess'
            '*.html'
            'views/{,*/}*.html'
            'bower_components/**/*'
            'images/{,*/}*.{webp}'
            'fonts/*'
          ]}
          {
          expand: true
          cwd: '.tmp/images'
          dest: '<%= yeoman.dist %>/images'
          src: ['generated/*']
          }]
      styles:
        expand: true
        cwd: '<%= yeoman.app %>/styles'
        dest: '.tmp/styles/'
        src: '{,*/}*.css'

    concurrent:
      server: [
        'coffee:dist'
        'jade'
        'copy:styles'
      ]
      test: [
        'coffee',
        'copy:styles'
      ]
      dist: [
        'coffee',
        'copy:styles',
        'imagemin',
        'svgmin'
      ]

    karma:
      unit:
        configFile: 'karma.conf.js'
        singleRun: true
  

  grunt.registerTask 'serve', (target) ->
    if target is 'dist'
      return grunt.task.run(['build', 'connect:dist:keepalive'])

    grunt.task.run [
      'clean:server',
      'bower-install',
      'concurrent:server',
      'autoprefixer',
      'connect:livereload',
      'watch'
    ]



  grunt.registerTask 'server', () ->
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve'])


  grunt.registerTask 'test', [
    'clean:server',
    'concurrent:test',
    'autoprefixer',
    'connect:test',
    'karma'
  ]

  grunt.registerTask 'build', [
    'clean:dist',
    'bower-install',
    'useminPrepare',
    'concurrent:dist',
    'jade',
    'autoprefixer',
    'concat',
    'ngmin',
    'copy:dist',
    'cdnify',
    'cssmin',
    'uglify',
    'rev',
    'usemin',
    'htmlmin'
  ]

  grunt.registerTask 'default', [
    'newer:jshint',
    'newer:coffeelint',
    'test',
    'build'
  ]

